1. Buat Database
 create database myshop;

2. Buat table
Buat table users
create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(225),
    -> email varchar(225),
    -> password varchar(225)
    -> );
Buat table categories
create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(225)
    -> );
Buat table items
create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(225),
    -> description varchar(225),
    -> price int(4),
    -> stock int(4),
    -> categories_id int(4),
    -> foreign key(categories_id) references categories(id)
    -> );
3. Insert data

- table users
insert into users(name,email,password) values
    -> ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");
Query OK, 2 rows affected (0.006 sec)

- table categories
insert into categories(name) values
    -> ("gadget"),("cloth"),("men"),("women"),("branded");
Query OK, 5 rows affected (0.004 sec)


-table items
insert into items(name,description,price,stock,categories_id) values
    -> ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
Query OK, 1 row affected (0.006 sec)

MariaDB [myshop]> insert into items(name,description,price,stock,categories_id) values
    -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);
Query OK, 1 row affected (0.005 sec)

MariaDB [myshop]> insert into items(name,description,price,stock,categories_id) values
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
Query OK, 1 row affected (0.006 sec)

4. Mengambil data dan Database
a. Mengambil data users
- select id, name, email from users;
b. mengambil data item pada tabel item dengan kata kunci "like" --> "uniklo", "watch", "sang"
- select *from items where price > 1000000;
- select *from items where name like "%sang%";
- select *from items where name like "uniklo%";
- select *from items where name like "%watch";
c. menampilkan data items join dengan kategori --> SQL joins w3school
- select items.name, items.description, items.price, items.stock, items.categories_id from
    -> items inner join categories on items.categories_id = categories.id;
 As --> mengganti nama kecuali
- select items.name, items.description, items.price, items.stock, items.categories_id As categori from
    -> items inner join categories on items.categories_id = categories.id;
5. Mengubah data dari database --> SQL update
- update items set price = 2500000 where id = 1;