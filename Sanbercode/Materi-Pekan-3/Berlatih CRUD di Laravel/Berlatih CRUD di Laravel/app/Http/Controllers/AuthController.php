<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function from()
    {
        return view('halaman.from');
    }

    public function wellcome(Request $request)
    {
        $name = $request['nama'];
        $name2 = $request['nama2'];


        return view('halaman.wellcome', compact('name', 'name2'));
    }

}
